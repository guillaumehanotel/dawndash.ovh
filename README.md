# dawndash

## Project setup

```
npm install
```

## Before serving the app, go to your host file

### For Linux :

```
sudo nano /etc/hosts
```

### For Windows :

Open :

> %SystemRoot%\System32\drivers\etc\hosts

### Then add this line in your host file :

```
127.0.0.1       me.mydomain.com
```

## Launch project

### Compiles and hot-reloads for development

```
npm run serve
```

Open this url in your browser :

> me.mydomain.com:8080

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).