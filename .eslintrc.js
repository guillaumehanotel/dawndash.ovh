module.exports = {
  root: true,

  env: {
    browser: true,
    node: true
  },

  parserOptions: {
    parser: "babel-eslint",
    ecmaVersion: 2020
  },

  // add your custom rules here
  rules: {
    "nuxt/no-cjs-in-config": "off",
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
  },

  extends: [
    "@nuxtjs",
    "plugin:nuxt/recommended",
    "vuetify",
    "plugin:vue/recommended",
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/prettier"
  ],
  ignorePatterns: ["dist/"],
};
