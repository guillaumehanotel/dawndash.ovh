const env = require("dotenv").config();

module.exports = {
  telemetry: false,
  ssr: false,
  env: env.parsed,
  server: {
    port: process.env.PORT,
    host: process.env.HOST
  },
  head: {
    titleTemplate:
      process.env.npm_package_name.charAt(0).toUpperCase() +
      process.env.npm_package_name.slice(1),
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  loading: {
    name: "chasing-dots",
    color: "#ff5638",
    background: "white",
    height: "4px"
  },
  css: ["~/assets/main.scss"],
  plugins: [
    {
      src: "@/plugins/persistedState.js",
      ssr: false
    },
    "@/plugins/axios.js",
    "@/plugins/directives.js",
    "@/plugins/vue-google-oauth2.js",
    "@/plugins/bus.js"
  ],
  buildModules: ["@nuxtjs/eslint-module", "@nuxtjs/vuetify"],
  modules: ["@nuxtjs/axios", "@nuxtjs/auth", "@nuxtjs/toast"],
  toast: {
    position: "top-right",
    duration: 2000
  },
  axios: {},
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    defaultAssets: {
      icons: "fa"
    }
  },
  build: {
    extend(config, ctx) {},
    transpile: ["vue-google-oauth2", "vue-clickaway"]
  },
  auth: {
    redirect: {
      login: "/login",
      logout: "/login",
      home: "/"
    }
  }
};
