import { normalize } from "normalizr";
import Vue from "vue";
import {
  getNewBookmarkOrderPosition,
  getNewTabOrderPosition,
  getTabsResponseSchema,
  handleRequestError
} from "../plugins/helpers";
import config from "../config";

export const strict = process.env.NODE_ENV !== "test";

export const state = () => {
  return {
    token: null,
    bookmarks: [],
    tabs: [],
    selectedTabIndex: null,
    selectedEditTabId: null,
    tabTransition: "",
    parentBookmarkId: null,
    newBookmarkMode: false,
    settings: null
  };
};

export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn;
  },
  user: state => state.auth.user,
  token: state => state.token,

  selectedTabIndex: state => state.selectedTabIndex,
  selectedEditTabId: state => state.selectedEditTabId,

  selectedTab: (state, getters) => {
    return getters.orderedTabs[state.selectedTabIndex];
  },
  selectedEditTab: state => {
    return state.tabs[state.selectedEditTabId];
  },
  bookmarksBySelectedTabAndParentFolder: (state, getters) => {
    if (getters.selectedTab) {
      const bookmarksIdsOfSelectedTab = getters.selectedTab.bookmarks;
      if (bookmarksIdsOfSelectedTab) {
        return Object.entries(state.bookmarks)
          .map(item => Object.assign({}, item[1]))
          .filter(item => bookmarksIdsOfSelectedTab.includes(item.id))
          .filter(item => item.parentBookmarkId === state.parentBookmarkId)
          .sort((a, b) => a.orderPosition - b.orderPosition);
      }
    }
    return [];
  },

  tabTransition: state => state.tabTransition,
  orderedTabs: state => {
    const unorderedTabs = state.tabs;
    const orderedKeys = Object.keys(unorderedTabs).sort(
      (a, b) => unorderedTabs[a].orderPosition - unorderedTabs[b].orderPosition
    );
    const orderedTabs = [];
    for (const key of orderedKeys) {
      orderedTabs.push(unorderedTabs[key]);
    }
    return orderedTabs;
  },

  isParentBookmarkExists: state => state.parentBookmarkId !== null,
  parentBookmarkId: state => state.parentBookmarkId,
  currentBookmarkFolder: state => {
    return Object.entries(state.bookmarks)
      .map(item => Object.assign({}, item[1]))
      .find(item => item.id === state.parentBookmarkId);
  },
  bookmarksByParentId: state => bookmarkId => {
    return Object.entries(state.bookmarks)
      .map(item => Object.assign({}, item[1]))
      .filter(item => item.parentBookmarkId === bookmarkId);
  },
  bookmarkHasChild: (state, getters) => bookmarkId => {
    return getters.bookmarksByParentId(bookmarkId).length !== 0;
  },
  firstBookmarksByParentId: (state, getters) => bookmarkId => {
    return getters
      .bookmarksByParentId(bookmarkId)
      .sort((a, b) => a.orderPosition - b.orderPosition)
      .slice(0, 4);
  },
  bookmarkPosition: state => bookmarkId => {
    return state.bookmarks[bookmarkId].orderPosition;
  },
  newBookmarkMode: state => state.newBookmarkMode,

  settings: state => state.settings
};

export const mutations = {
  SET_SELECTED_EDIT_TAB_ID(state, tabId) {
    state.selectedEditTabId = tabId;
  },
  SET_BOOKMARKS(state, bookmarks) {
    state.bookmarks = bookmarks;
  },
  ADD_BOOKMARK(state, { bookmark, tabId }) {
    Vue.set(state.bookmarks, bookmark.id, bookmark);
    if (state.tabs[tabId].bookmarks) {
      state.tabs[tabId].bookmarks.push(bookmark.id);
    } else {
      state.tabs[tabId].bookmarks = [bookmark.id];
    }
  },
  UPDATE_BOOKMARK(state, bookmark) {
    Vue.set(state.bookmarks, bookmark.id, bookmark);
  },
  DELETE_BOOKMARK(state, bookmarkId) {
    Vue.delete(state.bookmarks, bookmarkId);
  },
  SET_TABS(state, tabs) {
    state.tabs = tabs;
  },
  ADD_TAB(state, tab) {
    Vue.set(state.tabs, tab.id, tab);
  },
  UPDATE_TAB(state, tab) {
    Vue.set(state.tabs, tab.id, tab);
  },
  DELETE_TAB(state, tabId) {
    Vue.delete(state.tabs, tabId);
  },
  SET_SELECTED_TAB(state, tabIndex) {
    state.selectedTabIndex = tabIndex;
  },
  SET_TAB_TRANSITION(state, tabTransition) {
    state.tabTransition = tabTransition;
  },
  SET_PARENT_BOOKMARK_ID(state, bookmarkId) {
    state.parentBookmarkId = bookmarkId;
  },
  SET_TOKEN_ID(state, token) {
    state.token = token;
  },
  SET_NEW_BOOKMARK_MODE(state, newBookmarkMode) {
    state.newBookmarkMode = newBookmarkMode;
  },
  SET_SETTINGS(state, settings) {
    state.settings = settings;
  }
};

export const actions = {
  async createTab({ state, commit, getters }) {
    try {
      const tab = {
        title: "New Tab",
        orderPosition: getNewTabOrderPosition(getters.orderedTabs)
      };
      const response = await this.$axios.post(
        `${config.apiUrl}/users/${state.auth.user.id}/tabs`,
        tab
      );
      if (response.status === 201 && response.data.id) {
        tab.id = response.data.id;
        commit("ADD_TAB", tab);
        commit("SET_SELECTED_EDIT_TAB_ID", tab.id);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async updateTab({ state, commit, getters }, { tabId, title }) {
    try {
      const updatedTab = {
        ...state.tabs[tabId],
        title
      };
      const response = await this.$axios.put(
        `${config.apiUrl}/tabs/${tabId}`,
        updatedTab
      );
      if (response.status === 200) {
        commit("UPDATE_TAB", updatedTab);
        commit("SET_SELECTED_EDIT_TAB_ID", null);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async deleteTab({ state, commit, getters }, id) {
    try {
      const response = await this.$axios.delete(`${config.apiUrl}/tabs/${id}`);
      if (response.status === 204) {
        if (getters.selectedTab.id === id) {
          commit("SET_SELECTED_TAB", 0);
        }
        commit("DELETE_TAB", id);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  changeTab({ state, commit }, tabIndex) {
    const previousSelectedTabIndex = state.selectedTabIndex;
    commit("SET_SELECTED_TAB", tabIndex);
    const transition = tabIndex > previousSelectedTabIndex ? "next" : "prev";
    commit("SET_TAB_TRANSITION", transition);
    commit("SET_PARENT_BOOKMARK_ID", null);
  },

  async fetchBookmarksAndTabs({ state, commit, getters }) {
    try {
      const response = await this.$axios.$get(
        `${config.apiUrl}/users/${state.auth.user.id}/tabs`
      );
      const normalizedData = normalize(response, getTabsResponseSchema());
      commit("SET_BOOKMARKS", normalizedData.entities.bookmarks);
      commit("SET_TABS", normalizedData.entities.tabs);
      commit("SET_SELECTED_TAB", getters.orderedTabs[0]);
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async createFolderBookmark({ state, commit, getters }) {
    const folder = {
      title: "New Folder",
      url: null,
      thumbnailUrl: null,
      isLink: false,
      orderPosition: getNewBookmarkOrderPosition(
        getters.bookmarksBySelectedTabAndParentFolder
      ),
      tabId: getters.selectedTab.id,
      parentBookmarkId: state.parentBookmarkId
    };
    try {
      const response = await this.$axios.post(
        `${config.apiUrl}/bookmarks`,
        folder
      );
      if (response.status === 201 && response.data.id) {
        const selectedTabId = getters.selectedTab.id;
        folder.id = response.data.id;
        commit("SET_TAB_TRANSITION", "fade");
        commit("ADD_BOOKMARK", { bookmark: folder, tabId: selectedTabId });
        commit("SET_NEW_BOOKMARK_MODE", false);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async createLinkBookmark({ state, commit, getters }, url) {
    try {
      const link = {
        title: url,
        url,
        thumbnailUrl: null,
        isLink: true,
        orderPosition: getNewBookmarkOrderPosition(
          getters.bookmarksBySelectedTabAndParentFolder
        ),
        tabId: getters.selectedTab.id,
        parentBookmarkId: state.parentBookmarkId
      };
      const response = await this.$axios.post(
        `${config.apiUrl}/bookmarks`,
        link
      );
      if (response.status === 201 && response.data.id) {
        const selectedTabId = getters.selectedTab.id;
        link.id = response.data.id;
        link.thumbnailUrl = response.data.thumbnailUrl;
        link.title = response.data.title;
        commit("SET_TAB_TRANSITION", "fade");
        commit("ADD_BOOKMARK", { bookmark: link, tabId: selectedTabId });
        commit("SET_NEW_BOOKMARK_MODE", false);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async updateBookmark({ state, commit }, bookmark) {
    try {
      const response = await this.$axios.put(
        `${config.apiUrl}/bookmarks/${bookmark.id}`,
        bookmark
      );
      if (response.status === 200) {
        commit("UPDATE_BOOKMARK", bookmark);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async bulkUpdateBookmarks({ state, commit }, bookmarks) {
    try {
      const response = await this.$axios.patch(
        `${config.apiUrl}/bookmarks/bulk`,
        bookmarks
      );
      if (response.status === 200) {
        for (const bookmark of bookmarks) {
          commit("UPDATE_BOOKMARK", bookmark);
        }
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async deleteBookmark({ state, commit }, id) {
    try {
      const response = await this.$axios.delete(
        `${config.apiUrl}/bookmarks/${id}`
      );
      if (response.status === 204) {
        commit("SET_TAB_TRANSITION", "fade");
        commit("DELETE_BOOKMARK", id);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async updateSettings({ state, commit }, settings) {
    try {
      const response = await this.$axios.put(
        `${config.apiUrl}/settings/${settings.id}`,
        settings
      );
      if (response.status === 200) {
        commit("SET_SETTINGS", settings);
      }
    } catch (e) {
      handleRequestError(this, e);
    }
  },

  async importBookmarks({ state, commit }, formData) {
    try {
      formData.append("userId", state.auth.user.id);
      const response = await this.$axios.post(
        `${config.apiUrl}/bookmarks/import`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        }
      );
      console.log(response);
    } catch (e) {
      handleRequestError(this, e);
    }
  }
};
