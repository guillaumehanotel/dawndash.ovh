import Vuex from "vuex";
import { createLocalVue } from "@vue/test-utils";
import { mutations } from "./index";

describe("tabs", () => {
  const localVue = createLocalVue();
  localVue.use(Vuex);
  let NuxtStore;
  let store;

  beforeAll(async () => {
    // note the store will mutate across tests
    const storePath = `${process.env.buildDir}/store.js`;
    NuxtStore = await import(storePath);
  });

  beforeEach(async () => {
    store = await NuxtStore.createStore();
  });

  describe("SET_TABS", () => {
    it("adds tabs to the state", () => {
      const tabs = {
        1: {
          id: 1,
          orderPosition: 1,
          title: "Default"
        },
        2: {
          id: 2,
          orderPosition: 2,
          title: "Technologie"
        }
      };
      mutations.SET_TABS(store.state, tabs);
      expect(typeof store.state.tabs).toBe("object");
      expect(Object.keys(store.state.tabs).length).toEqual(2);
    });
  });

  // describe("orderedTabs", () => {
  //   let orderedTabs;
  //
  //   beforeEach(() => {
  //     orderedTabs = store.getters.orderedTabs;
  //   });
  //
  //   test("test is array", () => {
  //     console.log(orderedTabs);
  //     expect(_.isArray(orderedTabs)).toBe(true);
  //   });
  // });
});
