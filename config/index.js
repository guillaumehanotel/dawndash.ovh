export default {
  host: process.env.HOST || "localhost",
  port: process.env.PORT || "3000",
  apiUrl: process.env.API_URL || "http://dawndash.ovh/api",
  google: {
    clientId:
      process.env.GOOGLE_CLIENT_ID || "XXX-XXX.apps.googleusercontent.com"
  }
};
