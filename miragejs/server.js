import { Server, Response, Model } from "miragejs";
import config from "../config";

export function makeServer({ environment = "development" } = {}) {
  const server = new Server({
    environment,

    models: {
      user: Model
    },

    routes() {
      this.passthrough("https://www.googleapis.com/**");
      this.passthrough(`${config.apiUrl}/**`);
      this.passthrough(`${config.host}:${config.port}/**`);

      this.urlPrefix = `${config.host}:${config.port}`;
      this.namespace = "api";

      this.post("/tabs", (schema, request) => {
        const body = JSON.parse(request.requestBody);
        const responseData = {
          id: Math.floor(Math.random() * 1000) + 2200,
          ...body
        };
        return new Response(201, {}, responseData);
      });

      this.post("/bookmarks", (schema, request) => {
        const body = JSON.parse(request.requestBody);
        const responseData = {
          id: Math.floor(Math.random() * 1000) + 2200,
          ...body
        };
        if (body.is_link) {
          responseData.thumbnail_url = `https://picsum.photos/seed/${Math.floor(
            Math.random() * 1000
          ) + 2200}/300/200`;
        }
        return new Response(201, {}, responseData);
      });

      this.put("/bookmarks/:id", (schema, request) => {
        return new Response(200);
      });

      this.delete("/bookmarks/:id", (schema, request) => {
        return new Response(204);
      });

      this.get("/tabs", () => {
        return {
          tabs: [
            {
              id: 1,
              title: "Default",
              order_position: 1,
              bookmarks: [
                {
                  id: 1,
                  title: "Google",
                  url: "https://google.fr",
                  thumbnail_url:
                    "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_160x56dp.png",
                  is_link: true,
                  order_position: 2,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 11,
                  title: "Filled Folder",
                  url: null,
                  thumbnail_url: null,
                  is_link: false,
                  order_position: 3,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 2,
                  title: "Youtube",
                  url: "https://youtube.com",
                  thumbnail_url:
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/YouTube_full-color_icon_%282017%29.svg/1200px-YouTube_full-color_icon_%282017%29.svg.png",
                  is_link: true,
                  order_position: 1,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 3,
                  title: "Twitch",
                  url: "https://twitch.tv",
                  thumbnail_url:
                    "https://www.stickpng.com/assets/images/580b57fcd9996e24bc43c540.png",
                  is_link: true,
                  order_position: 2,
                  tab_id: 1,
                  parent_bookmark_id: 11
                },
                {
                  id: 4,
                  title: "Twitter",
                  url: "https://twitter.com",
                  thumbnail_url:
                    "https://upload.wikimedia.org/wikipedia/fr/thumb/c/c8/Twitter_Bird.svg/1200px-Twitter_Bird.svg.png",
                  is_link: true,
                  order_position: 4,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 5,
                  title: "Amazon",
                  url: "https://www.amazon.fr/",
                  thumbnail_url:
                    "https://pngimg.com/uploads/amazon/amazon_PNG27.png",
                  is_link: true,
                  order_position: 5,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 7,
                  title: "Ynov",
                  url: "https://sso.ynov.com/",
                  thumbnail_url: "https://i.imgur.com/Hn3UjD3.png",
                  is_link: true,
                  order_position: 6,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 12,
                  title: "Empty Folder",
                  url: null,
                  thumbnail_url: null,
                  is_link: false,
                  order_position: 7,
                  tab_id: 1,
                  parent_bookmark_id: null
                },
                {
                  id: 13,
                  title: "Empty Folder",
                  url: null,
                  thumbnail_url: null,
                  is_link: false,
                  order_position: 1,
                  tab_id: 1,
                  parent_bookmark_id: 11
                }
              ]
            },
            {
              id: 2,
              title: "Technology",
              order_position: 3,
              bookmarks: [
                {
                  id: 8,
                  title: "Stackoverflow",
                  url: "https://stackoverflow.com/",
                  thumbnail_url:
                    "https://img.icons8.com/color/480/stackoverflow.png",
                  is_link: true,
                  order_position: 1,
                  tab_id: 2,
                  parent_bookmark_id: null
                },
                {
                  id: 6,
                  title: "Github",
                  url: "https://github.com/",
                  thumbnail_url:
                    "https://github.githubassets.com/images/modules/logos_page/Octocat.png",
                  is_link: true,
                  order_position: 2,
                  tab_id: 2,
                  parent_bookmark_id: null
                }
              ]
            },
            {
              id: 3,
              title: "Work",
              order_position: 2,
              bookmarks: [
                {
                  id: 9,
                  title: "Google Drive",
                  url: "https://drive.google.com/drive/u/0/my-drive",
                  thumbnail_url:
                    "https://d5l07j6ktywkt.cloudfront.net/sdpreview/0/f155ec74/1377509910-521b2216b9f8d/preview.png",
                  is_link: true,
                  order_position: 1,
                  tab_id: 3,
                  parent_bookmark_id: null
                },
                {
                  id: 10,
                  title: "LinkedIn",
                  url: "https://www.linkedin.com",
                  thumbnail_url:
                    "https://d5l07j6ktywkt.cloudfront.net/sdpreview/0/591df28f/1377504659-521b0d93d57f5/preview.png",
                  is_link: true,
                  order_position: 2,
                  tab_id: 3,
                  parent_bookmark_id: null
                }
              ]
            }
          ]
        };
      });

      this.get("/user", (schema, request) => {
        const id = request.queryParams.google_uid;

        return schema.users.findBy({ google_uid: id });
      });

      this.post("/user", (schema, request) => {
        const attrs = JSON.parse(request.requestBody);

        return schema.users.create(attrs);
      });

      this.get("/users", schema => {
        return schema.users.all();
      });
    }
  });
  return server;
}
