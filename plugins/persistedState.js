import createPersistedState from "vuex-persistedstate";

export default ({ store }) => {
  createPersistedState({
    key: "arkvdlkmq6se5g468h4s6t8fg68trs1g3esg3d8r"
  })(store);
};
