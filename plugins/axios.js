export default function({ store, app: { $axios } }) {
  $axios.onRequest(config => {
    if (store.state.token) {
      const token = store.state.token;
      config.headers.common.Authorization = `${token.type} ${token.value}`;
    }
  });
}
