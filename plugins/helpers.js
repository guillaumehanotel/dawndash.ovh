import { schema } from "normalizr";

export const handleRequestError = function(context, exception) {
  if (exception.response) {
    const response = exception.response;
    if (response.status === 401) {
      context.$auth.logout();
    }
    console.error(response);
  } else {
    console.error(exception);
  }
};

export const getNewBookmarkOrderPosition = function(bookmarks) {
  if (bookmarks.length === 0) {
    return 1;
  }
  return Math.max(...bookmarks.map(item => item.orderPosition)) + 1;
};

export const getNewTabOrderPosition = function(tabs) {
  if (tabs.length === 0) {
    return 1;
  }
  return Math.max(...tabs.map(item => item.orderPosition)) + 1;
};

export const getTabsResponseSchema = function() {
  const bookmarkSchema = new schema.Entity("bookmarks");
  const tabSchema = new schema.Entity("tabs", {
    bookmarks: [bookmarkSchema]
  });
  return [tabSchema];
};

export const getUrlParameterByName = function(url, propertyName) {
  const urlParams = {};
  const queries = url.replace(/^\?/, "").split("&");
  for (let i = 0; i < queries.length; i++) {
    const split = queries[i].split("=");
    urlParams[split[0]] = split[1];
  }
  return urlParams[propertyName];
};

export const getOS = function() {
  const userAgent = window.navigator.userAgent;
  const platform = window.navigator.platform;
  const macosPlatforms = ["Macintosh", "MacIntel", "MacPPC", "Mac68K"];
  const windowsPlatforms = ["Win32", "Win64", "Windows", "WinCE"];
  const iosPlatforms = ["iPhone", "iPad", "iPod"];
  let os = null;

  if (macosPlatforms.includes(platform)) {
    os = "Mac OS";
  } else if (iosPlatforms.includes(platform)) {
    os = "iOS";
  } else if (windowsPlatforms.includes(platform)) {
    os = "Windows";
  } else if (/Android/.test(userAgent)) {
    os = "Android";
  } else if (!os && /Linux/.test(platform)) {
    os = "Linux";
  }

  return os;
};

export const readFileAsync = function(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.onerror = reject;
    reader.readAsText(file);
  });
};

export const isValidJsonString = function(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};
