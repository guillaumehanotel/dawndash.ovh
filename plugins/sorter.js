import interact from "interactjs";
import _ from "lodash";

class Sortable {
  constructor() {
    if (!Sortable.instance) {
      Sortable.instance = this;
    }
    return Sortable.instance;
  }

  initDraggableElements(element, newBookmarkPanel, onSwapCallback) {
    this.element = element;
    this.newBookmarkPanel = newBookmarkPanel;
    this.onSwapCallback = onSwapCallback;
    this.scrollable = document.body;
    if (this.element !== null) {
      this.items = this.element.querySelectorAll(this.element.dataset.sortable);
      this.setPositions();
      window.addEventListener(
        "resize",
        _.debounce(() => {
          this.setPositions();
        }, 30)
      );

      const onMove = _.throttle(
        e => {
          this.move(e);
        },
        16,
        { trailing: false }
      );

      interact(this.element.dataset.sortable, {
        context: this.element
      })
        .draggable({
          inertia: false,
          manualStart: false,
          autoScroll: {
            container: this.scrollable,
            margin: 150,
            speed: 600
          },
          onmove: onMove
        })
        .on("dragstart", e => {
          const r = e.target.getBoundingClientRect();
          e.target.classList.add("is-dragged");
          this.startPosition = e.target.dataset.position;
          this.offset = {
            x: e.clientX - r.left,
            y: e.clientY - r.top
          };
          this.scrollTopStart = this.scrollable.scrollTop;
        })
        .on("dragend", e => {
          e.target.classList.remove("is-dragged");
          this.moveItem(e.target, e.target.dataset.position);
          const newPositions = this.sendResults();
          this.onSwapCallback(newPositions);
        })
        .on("hold", e => {
          if (!e.interaction.interacting()) {
            e.interaction.start(
              {
                name: "drag"
              },
              e.interactable,
              e.currentTarget
            );
          }
        });
    }
  }

  setPositions(element) {
    console.log(element);
    let self = this;
    if (!(this instanceof Sortable)) {
      self = Sortable.instance;
    }
    if (element) {
      // calculer la diff avec les anciens items et mettre le nouveau en display none
      // pour ensuite le faire afficher à la bonne position en fade ?
      self.element = element;
      self.items = self.element.querySelectorAll(self.element.dataset.sortable);
      console.log(self.items);
    }
    const rect = self.items[0].getBoundingClientRect();
    self.itemWidth = Math.floor(rect.width);
    self.itemHeight = Math.floor(rect.height);
    self.cols = Math.floor(self.element.offsetWidth / self.itemWidth);
    self.element.style.height =
      self.itemHeight * Math.ceil(self.items.length / self.cols) + "px";
    for (let i = 0; i < self.items.length; i++) {
      const item = self.items[i];
      item.style.position = "absolute";
      item.style.top = "0px";
      item.style.left = "0px";
      item.style.transitionDuration = "0s";
      const position = item.dataset.position;
      self.moveItem(item, position);
    }
    self.moveItem(
      self.newBookmarkPanel,
      self.newBookmarkPanel.dataset.position
    );
    window.setTimeout(() => {
      for (let i = 0; i < self.items.length; i++) {
        const item = self.items[i];
        item.style.transitionDuration = null;
      }
    }, 100);
  }

  move(e) {
    const p = this.getXY(this.startPosition);
    const x = p.x + e.clientX - e.clientX0;
    const y =
      p.y +
      e.clientY -
      e.clientY0 +
      this.scrollable.scrollTop -
      this.scrollTopStart;
    e.target.style.transform = `translate3D(${x}px, ${y}px, 0)`;
    const oldPosition = e.target.dataset.position;
    const newPosition = this.guessPosition(
      x + this.offset.x,
      y + this.offset.y
    );
    if (oldPosition !== newPosition) {
      this.swap(oldPosition, newPosition);
      e.target.dataset.position = newPosition;
    }
  }

  getXY(position) {
    // console.log(this.cols);
    const x = this.itemWidth * (position % this.cols);
    const y = this.itemHeight * Math.floor(position / this.cols);
    return {
      x,
      y
    };
  }

  guessPosition(x, y) {
    let col = Math.floor(x / this.itemWidth);
    if (col >= this.cols) {
      col = this.cols - 1;
    }
    if (col <= 0) {
      col = 0;
    }
    let row = Math.floor(y / this.itemHeight);
    if (row < 0) {
      row = 0;
    }
    const position = col + row * this.cols;
    if (position >= this.items.length) {
      return this.items.length - 1;
    }
    return position;
  }

  swap(start, end) {
    for (let i = 0; i < this.items.length; i++) {
      const item = this.items[i];
      if (!item.classList.contains("is-dragged")) {
        const position = parseInt(item.dataset.position, 10);
        if (position >= end && position < start && end < start) {
          this.moveItem(item, position + 1);
        } else if (position <= end && position > start && start < end) {
          this.moveItem(item, position - 1);
        }
      }
    }
  }

  moveItem(item, position) {
    const p = this.getXY(position);
    item.style.transform = `translate3D(${p.x}px, ${p.y}px, 0)`;
    item.dataset.position = position;
  }

  sendResults() {
    const results = {};
    for (let i = 0; i < this.items.length; i++) {
      const item = this.items[i];
      results[parseInt(item.dataset.id, 10)] =
        parseInt(item.dataset.position, 10) + 1;
    }
    return results;
  }
}

const instance = new Sortable();
// Object.freeze(instance);

export default instance;
