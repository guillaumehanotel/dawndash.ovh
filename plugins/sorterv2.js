const SortableV2 = function(listWrapper, onUpdate) {
  // eslint-disable-next-line no-unused-vars
  let dragEl, nextEl, newPos;

  // eslint-disable-next-line no-unused-vars
  const oldPos = [...listWrapper.children].map(item => {
    item.draggable = true;
    return item.getBoundingClientRect();
    // return document.querySelector(item.id).getBoundingClientRect();
  });

  function _onDragOver(e) {
    e.preventDefault();
    e.dataTransfer.dropEffect = "move";

    let target = e.target;
    target = target.closest(".bookmark");
    if (target && target !== dragEl && target.nodeName === "DIV") {
      if (!target.classList.contains("bookmark")) {
        e.stopPropagation();
      } else {
        console.log("target", target.dataset.id);
        // getBoundinClientRect contains location-info about the element (relative to the viewport)
        const targetPos = target.getBoundingClientRect();
        // checking that dragEl is dragged over half the target y-axis or x-axis. (therefor the .5)
        const next =
          (e.clientY - targetPos.top) / (targetPos.bottom - targetPos.top) >
            0.5 ||
          (e.clientX - targetPos.left) / (targetPos.right - targetPos.left) >
            0.5;
        // console.log(next);
        // console.log(listWrapper);
        listWrapper.insertBefore(
          dragEl,
          (next && target.nextSibling) || target
        );

        /*  console.log("oldPos:" + JSON.stringify(oldPos));
         console.log("newPos:" + JSON.stringify(newPos)); */
        /* console.log(newPos.top === oldPos.top ? 'They are the same' : 'Not the same'); */
        // console.log(oldPos);
      }
    }
  }

  function _onDragEnd(evt) {
    evt.preventDefault();
    // newPos = [...listWrapper.children].map(child => {
    //   return document.getElementById(child.id).getBoundingClientRect();
    // });
    // console.log(newPos);
    dragEl.classList.remove("ghost");
    listWrapper.removeEventListener("dragover", _onDragOver, false);
    listWrapper.removeEventListener("dragend", _onDragEnd, false);

    if (nextEl !== dragEl.nextSibling) {
      onUpdate(dragEl);
    }
  }

  listWrapper.addEventListener("dragstart", function(e) {
    dragEl = e.target;
    nextEl = dragEl.nextSibling;
    /* dragGhost = dragEl.cloneNode(true);
    dragGhost.classList.add('hidden-drag-ghost'); */

    /*  document.body.appendChild(dragGhost);
     e.dataTransfer.setDragImage(dragGhost, 0, 0); */

    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.setData("Text", dragEl.textContent);

    listWrapper.addEventListener("dragover", _onDragOver, false);
    listWrapper.addEventListener("dragend", _onDragEnd, false);

    setTimeout(function() {
      dragEl.classList.add("ghost");
    }, 0);
  });
};

export default SortableV2;
