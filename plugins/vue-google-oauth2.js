import Vue from "vue";
import GAuth from "vue-google-oauth2";
import config from "../config";

const gauthOption = {
  clientId: config.google.clientId,
  scope: "profile email",
  prompt: "select_account"
};
Vue.use(GAuth, gauthOption);
